package com.card.api.CardAPI;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.card.api.CardAPI.dto.RequestDto;
import com.card.api.CardAPI.entity.CardInfo;
import com.card.api.CardAPI.repo.CardInfoRepo;
import com.card.api.CardAPI.util.PinUtil;
import com.card.api.CardAPI.util.SignatureUtil;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CardApiApplicationTests {
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private ObjectMapper mapper;
	@Autowired
	private CardInfoRepo cardInfoRepo;

	@Test
	public void post() throws IOException {
		RequestDto sendingData = new RequestDto();
		sendingData.setRequest_id("AIRPAY000001");
		List<String> test = new ArrayList<String>();
		test.add("000001");
		test.add("000002");
		sendingData.setReference_no_ls(test);
		sendingData.setProduct_code("VIETTEL100");
		sendingData.setPartner_id("AIRPAY");
		sendingData.setSubpartner_id("APA");
		sendingData.setSignature("LH5t6NgmhHYtKkOkKNTMzbImsgjOfC+AlWYHl++eb0G+EAU6ewkAZ0s51xwtTGY8jAsnT9Pn3E2bvm0U5OihbTvdaN0BHGDepvOfsNGVV01PGd4kFqdFcCcUdncfX0qe9s6eWE9W0kjpylq2oY5YKROJUv5Bo2V4C2q4MvjN/ZVRb8BgyunHqGhaDz3iI7vk+T0MrZEBXGGzhDDavdsPTCCkvk1ko4cyF4YIpP3/3tVvfSRl8CEFCM3zSg72v5mjms/DtT6Cb+rsSKZd5WF91fUY6ioa+oCV7uqqPj+moujps8/NFWp8/JlYyJsCQ2P8aRb7MUsEgnuj6aBrPTa5AQ==");
		String prm = mapper.writeValueAsString(sendingData);
		System.out.println("================================================");
		System.out.println("before: " + prm);
		System.out.println("================================================");
		try {
			String rtv = restTemplate.postForObject("http://localhost:8080/card/purchase_card", prm, String.class);
			System.out.println("================================================");
			System.out.println("after: " + rtv);
			System.out.println("================================================");
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void testGetSignatureByOurPrivateKey() throws Exception {
		RequestDto sendingData = new RequestDto();
		sendingData.setRequest_id("pay_test_00001");
		List<String> test = new ArrayList<String>();
		test.add("ved_000001");
		test.add("ved_000002");
		sendingData.setReference_no_ls(test);
		sendingData.setProduct_code("010_IVMS");
		sendingData.setPartner_id("AIRPAY");
		sendingData.setSubpartner_id("dev");
		
		ObjectMapper objMapper = new ObjectMapper();
		objMapper.setSerializationInclusion(Include.NON_NULL);
		
		String data = objMapper.writeValueAsString(sendingData);
		System.out.println("================================================");
		System.out.println("before: " + data);
		System.out.println("================================================");
		String sortedJson = SignatureUtil.getFinalSignatureAfterSigning(data);

		System.out.println("================================================");
		System.out.println("after: " + sortedJson);
		System.out.println("================================================");
	}
	
	@Test
	public void testVerifySignatureByTheirPublicKey() throws Exception {
		
		RequestDto sendingData = new RequestDto();
		sendingData.setRequest_id("pay_test_00001");
		List<String> test = new ArrayList<String>();
		test.add("ved_000001");
		test.add("ved_000002");
		sendingData.setReference_no_ls(test);
		sendingData.setProduct_code("010_IVMS");
		sendingData.setPartner_id("AIRPAY");
		sendingData.setSubpartner_id("dev");
		
		ObjectMapper objMapper = new ObjectMapper();
		objMapper.setSerializationInclusion(Include.NON_NULL);
		sendingData.setSignature("pXGnHOh31QZcQgKZNeC20epOk/JJcTto0kQ9HwWJUCDmfEKFCRn/ZQQ8DLNs5Oa1Im2pofpQRHaa0vW6J41/Qu4pLwsvyHzZEeVIKESxXmOdN2kVZkUvRbJMhlizhsIVo7nu5saw6t1C+wziqE6GJ1bynsySGPMa3+fXLB/1pJmIag6d+i8AD7dMecBeR+NQr8nGfp9IbJ0VP3blJAJetOtatjqRzLFqayXpqzrcy/ec8vp9qb3KVjrX2P9kJ05mH5Qhe+Tk8imu6h645usbklvKCOoTWYjH8QekSLU3RFGFufFm2I5VEnrt5q3glUfuDVzmGSwrp1wJEYpYAfx5ZA==");
//		sendingData.setSignature("a1LziF9XRzvmRw9xXCnakMOube+fhAWJTiK8vwUE+ncjNON6ZxdJ7OOa3ADM9nIAINw5B1cfB2jaHbt63hic0lv7Wzg6GFha2MLIcLb4npw30tjJKUMkRCS7LK9rBaE/QaN/v7A6IC/+7tkOjmr7CtR0cdREXDLaPF7gPtSWsEb+/+Q2KmLBypatS7FX5tIGl6dGBtG2CqO2vAPDZqP2oN8W5XDv6GCXKr4FWahcwxYnDSQpyGClSfQhktOzRUuFrxeRTonHllaTFhgcBMc5DLU93oVMgLbD0RycqGiHIcJT9b61aDpCRHRpPjBmv9ER0hfqv+W58jC/UAExegQmcg==");
		
		String data = objMapper.writeValueAsString(sendingData);
		
		System.out.println("================================================");
		System.out.println("before: " + data);
		System.out.println("================================================");
		boolean result = SignatureUtil.verifySignatureAndData(sendingData);

		System.out.println("================================================");
		System.out.println("after: " + String.valueOf(result));
		System.out.println("================================================");
		
	}
	
	@Test
	public void testPinCode() throws Exception {
		
		RequestDto request = new RequestDto();
		request.setPartner_id("AIRPAY_1");
		String pinCode = "2MJMFTHMJ2TJ";
		
		System.out.println("================================================");
		System.out.println("before pincode: " + pinCode);
		System.out.println("before requestDto: " + mapper.writeValueAsString(request));
		System.out.println("================================================");
		String result = PinUtil.getEncriptedPinCode(request, pinCode);

		System.out.println("================================================");
		System.out.println("after: " + String.valueOf(result));
		System.out.println("================================================");
		
	}
	
	@Test
	public void testGenSignature() throws JsonParseException, JsonMappingException, IOException {
		String data1 = "{\n" + 
				"\"request_id\": \"AIRPAY000001\", \"reference_no_ls\": [\"000001\", \"000002\"], \"partner_id\": \"AIRPAY\",\n" + 
				"\"subpartner_id\": \"APA\"}";
		
		RequestDto sendingData = mapper.readValue(data1, RequestDto.class);
		
		String data =mapper.writeValueAsString(sendingData);
		System.out.println("================================================");
		System.out.println("before: " + data);
		System.out.println("================================================");
		String sortedJson = SignatureUtil.getFinalSignatureAfterSigning(data);

		System.out.println("================================================");
		System.out.println("after: " + sortedJson);
		System.out.println("================================================");
	}
	
	@Test
	public void page() {
		Page<CardInfo> data = cardInfoRepo.findByCardNameAndUsedAndExpiredDateGreaterThanEqual("VIETTEL100",
				0, new Timestamp(System.currentTimeMillis()), PageRequest.of(0, 5));
		List<CardInfo> li = data.getContent();
		for (CardInfo cardInfo : li) {
			System.out.println(cardInfo.getId());
		}
	}
	
//	@Test
//	public void testResponseSignature() {
//		String signature = "sYQo4KWr4m2ZjhOhL/+eC27VImGLfy+ysdi2XeBzLNlCHr9s1eR7R7OerBJ346q1bYY8/r8dxNk6a2Meuw5Wvvno3ZkdQPrfwfj8WFbdsUUxJmAyAgw/f2FjBZ862bqfSNxEckc0/UhLDB6PUc8L+oqd2IibA0KgrMOAc+bsW2u+zZDeZnlWDRiO8+cuu6mMH4WLlZCfSgtY34JZaJcLoZBExa9l9MEXgRuP94Zt5f0qhwOY/WzerbYZEJ8Tc6UPqyzNmiHcXhMBGeJITm1PeMx+X1sntCcoyrcT0l0imzQZuwz2e7PRfRI6WBNfHiudZ2dCurSO71ocgaMh015iGg==";
//		String data = "{\"resp_code\":0,\"resp_msg\":\"Success\",\"data\":{\"reference\":\"1533037315999\",\"list_card\":[{\"reference_no\":\"ved_000001\",\"trans_time\":\"2018-07-31 18:41:55\",\"serial\":\"072791000000111\",\"pin\":\"aq994T8w/kI8LHSGJmOuiVWSNjYiYWeEPdHHI9BPo/pXonxiczhHcOj3crct3+KyzO1p4NZGim7UoBuYnBgSRqgq+SizUlJAZHdMJrtdh8Tm/NN9p3QyGsmJw986a623AH7inoZsVQYZEnG4EA9Zd5ornT4kmi+tkTifdv0Pn5Gn+l567fxKJusEtdLr7EuV7mmAnp9AJKCObXHOatADv69z9TRNzzU+nbdYlCK53XP1JS19WQws+tIfqR9rFqEnF6Jw1cOw8ghy6IbmrkaVgK5HT1CTRi09iUI+dr//LlVi/qXDO9adTVSMWKxBjvOmfNYAG7Wi99zvbskP4CFc6Q==\",\"expiry\":\"2020-12-31\",\"error\":0},{\"reference_no\":\"ved_000002\",\"trans_time\":\"2018-07-31 18:41:55\",\"serial\":\"072791000011111\",\"pin\":\"HxmUYjQiR27h+dXDrCDiE+d4qeXp7N3eGqzbnHRLSGAiJJ/DLbKa30KEnanf62624Vcea0mJX0pcfi/EWu55wJpSGU17Dt6Pfi+r3HKINYT6NUeiQ5XDfSMi0JVdzgPdWEB0VeLsewn782paGJDL16WA13wyvHUAKt1b0KutniHpUNMa75Jsx3q6guNOgBndME6cr4tYGXk03Xt0OIpfanxJjf5BUtOAs7LZKOIDJjRaFdObrcbItc8fWUZjppBv70UD8xEz09ZvAP8FgcUkYJmx+uYxhOl03M70staget2YC7CEBdzxUYl5/CEzIAaU5w128hrd68x8mbUh9TwApg==\",\"expiry\":\"2020-12-31\",\"error\":0}]}}";
//		System.out.println("================================================");
//		System.out.println("before: " + data);
//		System.out.println("================================================");
//		boolean result = SignatureUtil.testVerifySignature(data, signature);
//
//		System.out.println("================================================");
//		System.out.println("after: " + String.valueOf(result));
//		System.out.println("================================================");
//	}

}
