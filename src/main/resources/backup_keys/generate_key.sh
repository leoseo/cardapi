$ openssl genrsa -out keypair.pem 2048
Generating RSA private key, 2048 bit long modulus
............+++
................................+++
e is 65537 (0x10001)
$ openssl rsa -in keypair.pem -outform DER -pubout -out public.der
writing RSA key
$ openssl pkcs8 -topk8 -nocrypt -in keypair.pem -outform DER -out private.der

#https://stackoverflow.com/questions/5244129/use-rsa-private-key-to-generate-public-key
#Copied to gen a pub key
$ openssl rsa -in keypair.pem -pubout > keypair.pub
writing RSA key
