package com.card.api.CardAPI.repo;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.card.api.CardAPI.entity.CardInfo;

@Repository
public interface CardInfoRepo extends JpaRepository<CardInfo, Integer> {

	List<CardInfo> findCardInfoByReferenceNoInAndUsed(List<String> referenceNolist, Integer used);
	
	List<CardInfo> findCardInfoByReferenceNoIn(List<String> referenceNolist);

	Page<CardInfo> findByCardNameAndUsedAndExpiredDateGreaterThanEqual(String cardName, Integer used, Timestamp expiredDate, Pageable pageable);
	
	@Query(value="SELECT * FROM CardInfo c WHERE c.TransactionDate between :before and :after AND c.Used = :used",nativeQuery=true)
	List<CardInfo> getAllSuccessfulTransactionsForYesterday(@Param("before") String before, @Param("after") String after, @Param("used") Integer used);
	
}
