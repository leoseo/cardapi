package com.card.api.CardAPI.exception;

import java.util.ArrayList;
import java.util.List;

import com.card.api.CardAPI.dto.ResponseDataDto;
import com.card.api.CardAPI.dto.ResponseDto;
import com.card.api.CardAPI.util.CommonUtils;

public class BaseException extends RuntimeException {

	private static final long serialVersionUID = -7030732749416640448L;
	protected String message;
	protected Integer httpStatus;
	protected IErrorCode errorCode;
	protected String traceMessage;
	private List<FieldError> fieldErrors;

	public BaseException() {

		fieldErrors = new ArrayList<FieldError>();
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(Integer httpStatus) {
		this.httpStatus = httpStatus;
	}

	public IErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(IErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public String getTraceMessage() {
		return traceMessage;
	}

	public void setTraceMessage(String traceMessage) {
		this.traceMessage = traceMessage;
	}

	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(List<FieldError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	public List<FieldError> addFieldError(String fieldId, String errorCode, String errorMessage) {

		FieldError fieldError = new FieldError();
		fieldError.setFieldId(fieldId);
		fieldError.setErrorCode(errorCode);
		fieldError.setErrorMessage(errorMessage);

		fieldErrors.add(fieldError);

		return fieldErrors;
	}

	public List<FieldError> addFieldError(String fieldId, String errorCode, String errorMessage,
			Object[] errorMessageArgs) {

		FieldError fieldError = new FieldError();
		fieldError.setFieldId(fieldId);
		fieldError.setErrorCode(errorCode);
		fieldError.setErrorMessage(errorMessage);
		fieldError.setErrorMessageArgs(errorMessageArgs);

		fieldErrors.add(fieldError);

		return fieldErrors;
	}

	public ResponseDto transformToRestError() {

		ResponseDto restError = new ResponseDto();

		restError.setResp_code(errorCode.getResp_code());
		restError.setResp_msg(errorCode.getResp_msg());
		ResponseDataDto data = new ResponseDataDto();
		data.setReference(CommonUtils.generateRefence());
		restError.setData(data);

		return restError;
	}

}
