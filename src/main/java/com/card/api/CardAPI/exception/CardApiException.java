package com.card.api.CardAPI.exception;
import java.text.MessageFormat;
import java.util.List;

import com.card.api.CardAPI.dto.ResponseDto;

public class CardApiException extends BaseException {
	
	private static final long serialVersionUID = 8823356956725033191L;

	public CardApiException(IErrorCode ResponseConstant) {
		super();
		this.setErrorCode(ResponseConstant);
		this.setHttpStatus(ResponseConstant.getHttpStatus());
		this.setMessage(ResponseConstant.getResp_msg());
	}
	public CardApiException(IErrorCode cardApiErrorCode,String traceMessage) {
		super();
		this.setErrorCode(cardApiErrorCode);
		this.setTraceMessage(traceMessage);
		this.setMessage(cardApiErrorCode.getResp_msg());
	}
	public CardApiException(IErrorCode cardApiErrorCode,String traceMessage,Object ... arguments) {
		super();
		this.setErrorCode(cardApiErrorCode);
		this.setTraceMessage(traceMessage);
		String message = MessageFormat.format(cardApiErrorCode.getResp_msg(), arguments);
		this.setMessage(message);
	}
	
	public CardApiException(IErrorCode errorCodeEnum,List<FieldError> fieldErrors) {
		super();
		this.setErrorCode(errorCodeEnum);
		this.setFieldErrors(fieldErrors);
		this.setMessage(errorCodeEnum.getResp_msg());
	}
	public CardApiException(IErrorCode errorCodeEnum,List<FieldError> fieldErrors,String traceMessage) {
		super();
		this.setErrorCode(errorCodeEnum);
		this.setFieldErrors(fieldErrors);
		this.setTraceMessage(traceMessage);
		this.setMessage(errorCodeEnum.getResp_msg());
	}
	
	public CardApiException(final ResponseDto restError,final int httpStatus) {
		super();
		this.setErrorCode(new IErrorCode() {
			
			@Override
			public String getResp_msg() {
				return restError.getResp_msg();
			}
			
			@Override
			public Integer getHttpStatus() {
				return httpStatus;
			}
			
			@Override
			public Integer getResp_code() {
				return restError.getResp_code();
			}
		});
	}

}
