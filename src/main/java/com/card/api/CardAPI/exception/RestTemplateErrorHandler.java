package com.card.api.CardAPI.exception;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import com.card.api.CardAPI.dto.ResponseDto;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RestTemplateErrorHandler implements ResponseErrorHandler{
	private static Logger logger = LoggerFactory.getLogger(RestTemplateErrorHandler.class);
	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		HttpStatus status = response.getStatusCode();
		HttpStatus.Series series = status.series();
        return (HttpStatus.Series.CLIENT_ERROR.equals(series)
                || HttpStatus.Series.SERVER_ERROR.equals(series));
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException  {
		InputStream ips = response.getBody();
		StringWriter writer = new StringWriter();
		IOUtils.copy(ips, writer, "UTF-8");
		String restErrorStr =  writer.toString();
		logger.info("restErrorStr: {}",restErrorStr);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			ResponseDto restError = objectMapper.readValue(restErrorStr, ResponseDto.class);
			HttpStatus status = response.getStatusCode();
			Integer httpStatus = status.value();
			throw new CardApiException(restError, httpStatus);
		} catch (JsonParseException e) {
			logger.error(e.getMessage(),e);
		} catch (JsonMappingException e) {
			logger.error(e.getMessage(),e);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
	}

}
