package com.card.api.CardAPI.exception;
public interface IErrorCode {
	Integer  getResp_code();
	String  getResp_msg();
	Integer  getHttpStatus();
}
