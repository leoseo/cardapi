package com.card.api.CardAPI.exception;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.card.api.CardAPI.constant.ResponseConstant;
import com.card.api.CardAPI.dto.ResponseDataDto;
import com.card.api.CardAPI.dto.ResponseDto;
import com.card.api.CardAPI.util.CommonUtils;
import com.card.api.CardAPI.util.SignatureUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
/**
 * catching cardApiException, other exception will be caught by default exception handler  
 *
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice(annotations = RestController.class)
public class ControllerHandlerAdvice  extends ResponseEntityExceptionHandler {
	@Autowired
	private ObjectMapper mapper;
	private static Logger logger = LoggerFactory.getLogger(ControllerHandlerAdvice.class);
	// private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd
	// HH:mm:ss.SSS");

	@ExceptionHandler(CardApiException.class)
	public @ResponseBody ResponseDto handleCustomException(CardApiException cardApiex, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		logger.info("ExceptionHandelerAdvice:handle controller exception:{} <=> {}",cardApiex.getMessage(),cardApiex.getTraceMessage());

		logger.error(cardApiex.getMessage(), cardApiex);

		response.setStatus(cardApiex.getHttpStatus());

		ResponseDto restError = cardApiex.transformToRestError();
		
		return SignatureUtil.signResponseDto(restError,mapper);

	}
	
	@ExceptionHandler(Exception.class)
	public @ResponseBody String handleCustomException(Exception ex, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

		logger.info("ExceptionHandelerAdvice:handle controller exception:{} }",ex.getMessage());

		logger.error(ex.getMessage(), ex);

		response.setStatus(HttpServletResponse.SC_EXPECTATION_FAILED);

		ResponseDto restError = new ResponseDto();
		restError.setResp_code(ResponseConstant.UNEXPECTED_ERROR.getResp_code());
		restError.setResp_msg(ResponseConstant.UNEXPECTED_ERROR.getResp_msg());
		
		ResponseDataDto data = new ResponseDataDto();
		data.setReference(CommonUtils.generateRefence());
		restError.setData(data);
		
		String json = mapper.writeValueAsString(restError);
		
		String signature = SignatureUtil.getFinalSignatureAfterSigning(json);
		restError.setSignature(signature);
		
		return mapper.writeValueAsString(restError);

	}
}