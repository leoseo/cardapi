package com.card.api.CardAPI.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.card.api.CardAPI.exception.RestTemplateErrorHandler;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class ApplicationConfig {

	public static String rsaKeyPairPath;
	public static String defaultPartnerKeyPath;
	public static String publicKeyName;
	public static String privateKeyNameDer;
	public static String privateKeyNamePem;
	public static String ftpServerDomain;
	public static Integer ftpServerPort;
	public static String ftpUsername;
	public static String ftpPassword;
	public static String storedFilePath;
	public static String partnerName;

	@Value("${rsa.keypair}")
	public void setRsaKeyPairPath(String name) {
		rsaKeyPairPath = name;
	}
	@Value("${default.partner.path}")
	public void setDefaultPartnerKeyPath(String name) {
		defaultPartnerKeyPath = name;
	}
	@Value("${public.key.name}")
	public void setPublicKeyName(String name) {
		publicKeyName = name;
	}
	@Value("${private.key.name.der}")
	public void setPrivateKeyNamePCKS8(String name) {
		privateKeyNameDer = name;
	}
	@Value("${private.key.name.pem}")
	public void setPrivateKeyNamePCKS1(String name) {
		privateKeyNamePem = name;
	}
	@Value("${ftp.server.domain}")
    public void setFTPServerDomain(String server) {
    		ftpServerDomain = server;
    }
    @Value("${ftp.server.port}")
    public void setFTPServerPort(Integer port) {
    		ftpServerPort = port;
    }
	@Value("${ftp.username}")
	public void setFTPUsername(String userName) {
		ftpUsername = userName;
	}
	@Value("${ftp.password}")
	public void setFTPPassword(String password) {
		ftpPassword = password;
	}
	@Value("${local.stored.path}")
	public void setLocalStoredPath(String path) {
		storedFilePath = path;
	}
	@Value("${partner.name}")
	public void setPartnerName(String name) {
		partnerName = name;
	}

	@Bean(name = "jacksonObjectMapper")
	public ObjectMapper getObjectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}

	@Bean(name = "resttemplate")
	public RestTemplate getRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new RestTemplateErrorHandler());
		return restTemplate;
	}

}

