package com.card.api.CardAPI.configs;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.card.api.CardAPI.constant.Constant;
import com.card.api.CardAPI.service.CardService;

@Configuration
@EnableAsync
@EnableScheduling
public class TaskExecutor {
	
	private static final Logger logger = LoggerFactory.getLogger(TaskExecutor.class);

	@Autowired
	CardService cardService;
	
	@Value("${report.task.cron.expression}")
	private String cronExpression;
	
	@Scheduled(cron = "${report.task.cron.expression}")
	public void uploadFileToFTPServer() {
		Date now = new Date();
		String nowInString = new SimpleDateFormat(Constant.FULL_TIME_FORMAT).format(now);
		logger.info("Scheduled task - Extracting today records and upload to FTP server using cron jobs - "
				+ nowInString + " and cronExpression: " + cronExpression);
		cardService.reportAllSuccessfulTransactionsForYesterday();
		logger.info("Done running job for {}", nowInString);
	}

}
