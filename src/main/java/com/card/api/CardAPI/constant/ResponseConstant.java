package com.card.api.CardAPI.constant;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.card.api.CardAPI.exception.IErrorCode;

public enum ResponseConstant implements IErrorCode {
	
	// used for sending signal to AIRPAY
	SUCCESS("Success", 0), PARAMS_INVALID("Params_invalid", 1),
	AUTHENTICATION_ERROR("Authentication_error", 2), QUANTITY_INVALID("Quantity_invalid", 3),
	REFERENCE_NOT_FOUND("Reference_not_found", 4), REFERENCE_DUPLICATED("Reference_duplicated", 5),
	OUT_OF_STOCK("Out_of_stock", 6), PARTNER_NOT_FOUND("Partner_not_found", 7),
	PRODUCT_NOT_FOUND_ERROR("Product_not_found_error", 8), UNEXPECTED_ERROR("Unexpected_error", 10),
	
	// used internally. DO NOT THROW it for airpay's request
	FILE_SAVE_ERROR("File_save_error", 1000), FILE_UPLOAD_ERROR("File_upload_error", 1001),
	FILE_LOAD_ERROR("File_load_error", 1002)
	;

	// lookup table to be used to find enum for conversion
	private static final Map<Integer, ResponseConstant> lookup = new HashMap<Integer, ResponseConstant>();
	static {
		for (ResponseConstant e : EnumSet.allOf(ResponseConstant.class))
			lookup.put(e.getResp_code(), e);
	}

	private Integer errorCode;
	private String messageCode;
	private Integer httpStatus;

	ResponseConstant(String messageCode,Integer errorCode) {
		this.errorCode = errorCode;
		this.messageCode = messageCode;
		this.httpStatus = HttpServletResponse.SC_BAD_REQUEST;
	}

	ResponseConstant(String messageCode,Integer errorCode,Integer httpStatus) {
		this.errorCode = errorCode;
		this.messageCode = messageCode;
		this.httpStatus = httpStatus;
	}

	public Integer getResp_code() {
		return this.errorCode;
	}

	public static ResponseConstant get(Integer errorCode) {
		return lookup.get(errorCode);
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getResp_msg() {
		return messageCode;
	}

	public Integer getHttpStatus() {
		return httpStatus;
	}

	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	public void setHttpStatus(Integer httpStatus) {
		this.httpStatus = httpStatus;
	}

}
