package com.card.api.CardAPI.constant;

public class Constant {
	
	public final static String FULL_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss:SSS";
	public final static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public final static String ANOTHER_DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";
	public final static String DATE_FORMAT = "yyyy-MM-dd";
	public final static String PLAIN_DATE_FORMAT = "yyyyMMdd";

}
