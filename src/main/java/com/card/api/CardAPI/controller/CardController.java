package com.card.api.CardAPI.controller;

//import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.card.api.CardAPI.dto.RequestDto;
import com.card.api.CardAPI.dto.ResponseDto;
//import com.card.api.CardAPI.entity.CardInfo;
import com.card.api.CardAPI.repo.CardInfoRepo;
import com.card.api.CardAPI.service.CardService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
//@RequestMapping("/card")
public class CardController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ObjectMapper mapper;
	
	@Autowired
	CardService cardService;

	@Autowired
	CardInfoRepo cardInfoRepo;

	@RequestMapping(value = "/purchase_card", method = RequestMethod.POST)
	public @ResponseBody ResponseDto pucharseCard(@RequestBody RequestDto params) {
		logger.info("Entered /purchase_card with params {}", params.toString());
		return cardService.getResultAfterSendingCardPurcharsingRequest(params);
	}

	@RequestMapping(value = "/get_card", method = RequestMethod.POST)
	public @ResponseBody ResponseDto getCard(@RequestBody RequestDto params) {
		logger.info("Entered /get_card with params {}", params.toString());
		return cardService.getResultAfterSendingCardGettingRequest(params);
	}

	@RequestMapping(value = "/refund_card", method = RequestMethod.POST)
	public @ResponseBody ResponseDto refundCard(@RequestBody RequestDto params) {
		logger.info("Entered /refund_card with params {}", params.toString());
		return cardService.getResultAfterSendingCardRefundRequest(params);
		
	}

//	@RequestMapping(value = "/all", method = RequestMethod.GET)
//	public @ResponseBody List<CardInfo> all() {
//		List<CardInfo> data = cardInfoRepo.findAll();
//		return data;
//	}
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public @ResponseBody String testJob() {
		cardService.reportAllSuccessfulTransactionsForYesterday();
		return "OK";
	}
}
