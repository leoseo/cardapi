package com.card.api.CardAPI.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
@Entity
@Table(name="CardInfo")
@NamedQuery(name="CardInfo.findAll", query="SELECT c FROM CardInfo c")
public class CardInfo implements Serializable{
	
	private static final long serialVersionUID = -1823199790216597398L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private int id;
	
	@Column(name="CardName")
	private String cardName;
	
	@Column(name="UnitPrice")
	private Integer unitPrice;
	
	@Column(name="SerialNumber")
	private String serialNumber;
	
	@Column(name="PinCode")
	private String pinCode;
	
	@Column(name="ExpiredDate")
	private Timestamp expiredDate;
	
	@Column(name="Used")
	private Integer used;
	
	@Column(name="TransactionDate")
	private Timestamp transactionDate;
	
	@Column(name="RequestedPartnerId")
	private String requestedPartnerId;
	
	@Column(name="ReferenceNo")
	private String referenceNo;
	
	@Column(name="RequestedSubPartnerId")
	private String requestedSubPartnerId;
	
	@Column(name="PartnerReferenceNo")
	private String partnerReferenceNo;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public Timestamp getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Timestamp expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Integer getUsed() {
		return used;
	}

	public void setUsed(Integer used) {
		this.used = used;
	}

	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getRequestedPartnerId() {
		return requestedPartnerId;
	}

	public void setRequestedPartnerId(String requestedPartnerId) {
		this.requestedPartnerId = requestedPartnerId;
	}
	
	public String getReferenceNo() {
		return referenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}

	public String getRequestedSubPartnerId() {
		return requestedSubPartnerId;
	}

	public void setRequestedSubPartnerId(String requestedSubPartnerId) {
		this.requestedSubPartnerId = requestedSubPartnerId;
	}

	public String getPartnerReferenceNo() {
		return partnerReferenceNo;
	}

	public void setPartnerReferenceNo(String partnerReferenceNo) {
		this.partnerReferenceNo = partnerReferenceNo;
	}

}
