package com.card.api.CardAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan(basePackages={"com.card.api.CardAPI"})
//@PropertySource(value = { "classpath:configs.properties" })
@EnableTransactionManagement
@EntityScan(basePackages="com.card.api.CardAPI.entity")
@EnableJpaRepositories(basePackages="com.card.api.CardAPI.repo")
public class CardApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardApiApplication.class, args);
	}
}
