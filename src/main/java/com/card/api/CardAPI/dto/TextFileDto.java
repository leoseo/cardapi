package com.card.api.CardAPI.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TextFileDto {
	
	private String TranTime;
	private String Product_code;
	private Integer Amount;
	private String Reference_No_Partner;
	private String Reference_No;
	private String AirPay_ResponseCode;
	private String MismatchedData;
	private String Checksum;

}
