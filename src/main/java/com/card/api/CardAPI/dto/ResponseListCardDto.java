package com.card.api.CardAPI.dto;

public class ResponseListCardDto {
	
	private String reference_no;
	private String trans_time;
	private String serial;
	private String pin;
	private String expiry;
	private Integer error;
	
	public ResponseListCardDto (String reference_no, String trans_time, String serial,
			String pin, String expiry, Integer error) {
		this.reference_no = reference_no;
		this.trans_time = trans_time;
		this.serial = serial;
		this.pin = pin;
		this.expiry = expiry;
		this.error = error;
	}
	public ResponseListCardDto (String reference_no, String serial, Integer error) {
		this.reference_no = reference_no;
		this.serial = serial;
		this.error = error;
	}
	public String getReference_no() {
		return reference_no;
	}
	public void setReference_no(String reference_no) {
		this.reference_no = reference_no;
	}
	public String getTrans_time() {
		return trans_time;
	}
	public void setTrans_time(String trans_time) {
		this.trans_time = trans_time;
	}
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getExpiry() {
		return expiry;
	}
	public void setExpiry(String expiry) {
		this.expiry = expiry;
	}
	public Integer getError() {
		return error;
	}
	public void setError(Integer error) {
		this.error = error;
	}

}
