package com.card.api.CardAPI.dto;

import java.util.List;

public class ResponseDataDto {
	
	private String reference;
	private List<ResponseListCardDto> list_card;
	
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public List<ResponseListCardDto> getList_card() {
		return list_card;
	}
	public void setList_card(List<ResponseListCardDto> list_card) {
		this.list_card = list_card;
	}

}
