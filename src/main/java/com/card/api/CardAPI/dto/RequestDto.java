package com.card.api.CardAPI.dto;

import java.util.List;

import org.apache.commons.lang.StringUtils;

public class RequestDto {
	
	private String request_id;
	private String product_code;
	private List<String> reference_no_ls;
	private String partner_id;
	private String subpartner_id;
	private String signature;
	
	public String getRequest_id() {
		return request_id;
	}
	public void setRequest_id(String request_id) {
		this.request_id = request_id;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public List<String> getReference_no_ls() {
		return reference_no_ls;
	}
	public void setReference_no_ls(List<String> reference_no_ls) {
		this.reference_no_ls = reference_no_ls;
	}
	public String getPartner_id() {
		return partner_id;
	}
	public void setPartner_id(String partner_id) {
		this.partner_id = partner_id;
	}
	public String getSubpartner_id() {
		return subpartner_id;
	}
	public void setSubpartner_id(String subpartner_id) {
		this.subpartner_id = subpartner_id;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	@Override
	public String toString() {
		return "RequestDto [" 
				+ "request_id=" + getCheckedString(request_id)
				+ ",product_code=" + getCheckedString(product_code)
				+ ",reference_no_ls=" 
					+ (!reference_no_ls.isEmpty() ? reference_no_ls.size() : "0")
				+ ",partner_id=" + getCheckedString(partner_id)
				+ ",subpartner_id=" + getCheckedString(subpartner_id)
				+ ",signature=" + getCheckedString(signature)
				+ "]";
	}
	
	private String getCheckedString (String input) {
		return !StringUtils.isBlank(input) ? input : "null";
	}

}
