package com.card.api.CardAPI.dto;

public class ResponseDto {
	
	private Integer resp_code;
	private String resp_msg;
	private ResponseDataDto data;
	private String signature;
	
	public Integer getResp_code() {
		return resp_code;
	}
	public void setResp_code(Integer resp_code) {
		this.resp_code = resp_code;
	}
	public String getResp_msg() {
		return resp_msg;
	}
	public void setResp_msg(String resp_msg) {
		this.resp_msg = resp_msg;
	}
	public ResponseDataDto getData() {
		return data;
	}
	public void setData(ResponseDataDto data) {
		this.data = data;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}

}
