package com.card.api.CardAPI.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.card.api.CardAPI.constant.Constant;
import com.card.api.CardAPI.constant.ResponseConstant;
import com.card.api.CardAPI.dto.RequestDto;
import com.card.api.CardAPI.dto.ResponseDataDto;
import com.card.api.CardAPI.dto.ResponseDto;
import com.card.api.CardAPI.dto.ResponseListCardDto;
import com.card.api.CardAPI.entity.CardInfo;
import com.card.api.CardAPI.exception.CardApiException;
import com.card.api.CardAPI.repo.CardInfoRepo;
import com.card.api.CardAPI.util.FileUtils;
import com.card.api.CardAPI.util.LoggingUtils;
import com.card.api.CardAPI.util.PinUtil;
import com.card.api.CardAPI.util.SignatureUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class CardServiceImpl implements CardService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	ObjectMapper mapper;
	
	@Autowired
	CardInfoRepo cardInfoRepo;

	@Override
	public ResponseDto getResultAfterSendingCardPurcharsingRequest(RequestDto params) {
		
		try {
			
			// checking public key
			if (!SignatureUtil.verifySignatureAndData(params)) {
				throw new CardApiException(ResponseConstant.AUTHENTICATION_ERROR);
			}
			
			// checking param
			if (params == null) {
				throw new CardApiException(ResponseConstant.PARAMS_INVALID);
			}
			
			// checking productCode
			String productCode = params.getProduct_code();
			if (StringUtils.isEmpty(productCode)) {
				throw new CardApiException(ResponseConstant.PRODUCT_NOT_FOUND_ERROR);
			}
			logger.info("productCode: {}", productCode);
			
			// checking buying quantity
			List<String> referenceNos = params.getReference_no_ls();
			if (referenceNos == null || referenceNos.isEmpty()) {
				throw new CardApiException(ResponseConstant.QUANTITY_INVALID);
			}
			logger.info("reference_no_ls is not null with size {}",
					productCode, referenceNos.size());
			
			// Initialize default response
			Date date = Calendar.getInstance().getTime();
			ResponseDto response = getDefaultResponse(params,date);
			
			// prepare for find duplicates
			List<CardInfo> usedReferenceCards = cardInfoRepo.findCardInfoByReferenceNoIn(referenceNos);
			boolean duplicateDetected = 
					(usedReferenceCards == null || usedReferenceCards.isEmpty()) 
					? false : true;
			logger.info("duplicateDetected: {}", String.valueOf(duplicateDetected));
			
			Page<CardInfo> availableCardPages = cardInfoRepo.findByCardNameAndUsedAndExpiredDateGreaterThanEqual
					(productCode, UNUSED, new Timestamp(date.getTime()), PageRequest.of(0, referenceNos.size()));
			// checking available stocks
			if (availableCardPages == null || availableCardPages.getContent() == null 
					|| availableCardPages.getContent().isEmpty()) {
				throw new CardApiException(ResponseConstant.OUT_OF_STOCK);
			}
			
			List<CardInfo> availableCards = availableCardPages.getContent();
			logger.info("Found total [" + availableCards.size() + "] available cards");
			ResponseDataDto responseData = response.getData();
			List<ResponseListCardDto> listCards = responseData.getList_card();
			
			// looping by this size (larger)
			int duplicatedCount = 0;
			for (int i = 0; i < referenceNos.size(); i++) {
				// get reference no
				ResponseListCardDto responseListCard = null;
				String referenceNo = referenceNos.get(i);
				
				if (duplicateDetected) {
					// check duplicated reference
					CardInfo duplicateData = usedReferenceCards
							.stream()
							.filter(x -> referenceNo.equals(x.getReferenceNo()))
							.findAny().orElse(null);
					if (duplicateData != null) {
						duplicatedCount ++;
						if (duplicatedCount < referenceNos.size()) {
							responseListCard = new ResponseListCardDto(referenceNo,
									null, null, null, null, 
									ResponseConstant.REFERENCE_DUPLICATED.getResp_code());
							logger.info("Detecting REFERENCE_DUPLICATED - setting responseListCard: {}",
									mapper.writeValueAsString(responseListCard));
							listCards.add(responseListCard);
							continue;
						} else {
							// only allow proceeding if part of it is duplicated
							throw new CardApiException(ResponseConstant.REFERENCE_DUPLICATED); 
						}
					}
				}
				
				// no more cards
				if (i >= availableCards.size()) {
					// now begin setting response
					responseListCard = new ResponseListCardDto(referenceNo,
							null, null, null, null, 
							ResponseConstant.OUT_OF_STOCK.getResp_code());
					logger.info("Detecting OUT_OF_STOCK - setting responseListCard: {}",
							mapper.writeValueAsString(responseListCard));
					listCards.add(responseListCard);
					continue;
					
				}
				
				// OK - get each card info
				CardInfo availableCard = availableCards.get(i);
				String pinCode = availableCard.getPinCode();
				String serialNumber = availableCard.getSerialNumber();
				Timestamp expiredDate = availableCard.getExpiredDate();
				
				// setting used
				logger.info("Marking used, transaction date and requestedPartnerId for card serialNumber: {}", serialNumber);
				availableCard.setUsed(USED);
				availableCard.setReferenceNo(referenceNo);
				availableCard.setTransactionDate(new Timestamp(date.getTime()));
				availableCard.setRequestedPartnerId(params.getPartner_id());
				availableCard.setRequestedSubPartnerId(params.getSubpartner_id());
				availableCard.setPartnerReferenceNo(responseData.getReference());

				// now begin setting response
				responseListCard = new ResponseListCardDto(referenceNo,
						getTransactionTimeInString(date), serialNumber, pinCode,
						getExpiryTime(expiredDate), ResponseConstant.SUCCESS.getResp_code());
				logger.info("Setting responseListCard: {}", mapper.writeValueAsString(responseListCard));

				// changing pincode to base64
				String newPinCode = PinUtil.getEncriptedPinCode(params, pinCode);
				responseListCard.setPin(newPinCode);
				logger.info("Using new Base64 Pin: {}", newPinCode);
				
				listCards.add(responseListCard);
			}
			
			// set signature for success cases only.
			// Exception handler is responsible for signing failed cases
			return SignatureUtil.signResponseDto(response, mapper);
		} catch (CardApiException ex) {
			logger.error("Caught CardApiException !!" + ex.getMessage(), ex);
			throw ex; 
		} catch (Exception ex) {
			logger.error("Caught exception !!" + ex.getMessage(), ex);
			// default
			throw new CardApiException(ResponseConstant.AUTHENTICATION_ERROR); 
		}
	}

	@Override
	public ResponseDto getResultAfterSendingCardGettingRequest(RequestDto params) {
		try {
			
			// checking public key
			if (!SignatureUtil.verifySignatureAndData(params)) {
				throw new CardApiException(ResponseConstant.AUTHENTICATION_ERROR);
			}
			
			// checking param
			if (params == null) {
				throw new CardApiException(ResponseConstant.PARAMS_INVALID);
			}
			
			List<String> referenceNos = params.getReference_no_ls();
			if (referenceNos == null || referenceNos.isEmpty()) {
				throw new CardApiException(ResponseConstant.QUANTITY_INVALID);
			}
			logger.info("reference_no_ls is not null with size {}", referenceNos.size());
			
			List<CardInfo> soldCards = cardInfoRepo.findCardInfoByReferenceNoInAndUsed(params.getReference_no_ls(),
					USED);
			if (soldCards == null || soldCards.isEmpty()) {
				throw new CardApiException(ResponseConstant.REFERENCE_NOT_FOUND);
			}
			
			// Initialize default response
			Date date = Calendar.getInstance().getTime();
			ResponseDto response = getDefaultResponse(params, date);
			
			logger.info("Found total [" + soldCards.size() + "] sold cards");
			ResponseDataDto responseData = response.getData();
			
			List<ResponseListCardDto> listCards = responseData.getList_card();
			for (String referenceNo: referenceNos) {
				
				// checking existence
				CardInfo soldCard = soldCards
						.stream()
						.filter(p -> referenceNo.equals(p.getReferenceNo()))
						.findFirst().orElse(null);
				ResponseListCardDto responseListCard = null;
				if (soldCard == null) {
					// found nothing
					responseListCard = new ResponseListCardDto(referenceNo,
							null, null, null, null, 
							ResponseConstant.REFERENCE_NOT_FOUND.getResp_code());
					logger.info("Detecting REFERENCE_NOT_FOUND - setting responseListCard: {}",
							mapper.writeValueAsString(responseListCard));
					listCards.add(responseListCard);
					continue;
				}
				
				// OK - get each card info
				String pinCode = soldCard.getPinCode();
				String serialNumber = soldCard.getSerialNumber();
				Timestamp expiredDate = soldCard.getExpiredDate();
				Date transactionDate = new Date(soldCard.getTransactionDate().getTime());
				
				// now begin setting response
				responseListCard = new ResponseListCardDto(referenceNo,
						getTransactionTimeInString(transactionDate), serialNumber, pinCode,
						getExpiryTime(expiredDate), ResponseConstant.SUCCESS.getResp_code());
				logger.info("Setting responseListCard: {}", mapper.writeValueAsString(responseListCard));
				
				// changing pincode to base64
				String newPinCode = PinUtil.getEncriptedPinCode(params, pinCode);
				responseListCard.setPin(newPinCode);
				logger.info("Using new Base64 Pin: {}", newPinCode);
				listCards.add(responseListCard);
			}
			
			// set signature for success cases only.
			// Exception handler is responsible for signing failed cases
			return SignatureUtil.signResponseDto(response, mapper);
		} catch (CardApiException ex) {
			logger.error("Caught CardApiException !!" + ex.getMessage(), ex);
			throw ex; 
		} catch (Exception ex) {
			logger.error("Caught exception !!" + ex.getMessage(), ex);
			// default
			throw new CardApiException(ResponseConstant.AUTHENTICATION_ERROR); 
		}
	}

	@Override
	public ResponseDto getResultAfterSendingCardRefundRequest(RequestDto params) {
		try {
			
			if (!SignatureUtil.verifySignatureAndData(params)) {
				throw new CardApiException(ResponseConstant.AUTHENTICATION_ERROR);
			}
			
			// checking param
			if (params == null) {
				throw new CardApiException(ResponseConstant.PARAMS_INVALID);
			}
			
			List<String> referenceNos = params.getReference_no_ls();
			if (referenceNos == null || referenceNos.isEmpty()) {
				throw new CardApiException(ResponseConstant.QUANTITY_INVALID);
			}
			logger.info("referenceNos list is {}", params.getReference_no_ls().toString());
			
			List<CardInfo> data = cardInfoRepo.findCardInfoByReferenceNoInAndUsed(referenceNos,
					USED);
			if (data.isEmpty()) {
				throw new CardApiException(ResponseConstant.REFERENCE_NOT_FOUND);
			}
			logger.info("refund data is {}", LoggingUtils.objToStringIgnoreEx(data));
			
			// Initialize default response
			Date date = Calendar.getInstance().getTime();
			ResponseDto response = getDefaultResponse(params, date);
			ResponseDataDto responseData = response.getData();
			List<ResponseListCardDto> listCards = responseData.getList_card();
			for (String referenceNo : params.getReference_no_ls()) {
				logger.info("find reference no:{} size:{}", referenceNo, data.size());
				CardInfo cardInfo = data
						.stream()
						.filter(p -> referenceNo.equals(p.getReferenceNo())).findAny().orElse(null);
				
				ResponseListCardDto responseListCard = null;
				if (cardInfo == null) {
					responseListCard = new ResponseListCardDto(referenceNo, null,
							ResponseConstant.REFERENCE_NOT_FOUND.getResp_code());
					logger.info("Detecting REFERENCE_NOT_FOUND - setting responseListCard: {}",
							mapper.writeValueAsString(responseListCard));
					listCards.add(responseListCard);
					continue;
				}
				
				// OK - building response
				String serialNumber = cardInfo.getSerialNumber();
				responseListCard = new ResponseListCardDto(referenceNo,
						serialNumber, ResponseConstant.SUCCESS.getResp_code());
				
				//change cardinfo to unused=>save to database
				logger.info("Marking refunded status for card serialNumber: {}", serialNumber);
				cardInfo.setPartnerReferenceNo(responseData.getReference());
				cardInfo.setUsed(REFUNDED);
				
				listCards.add(responseListCard);
			}

			// set signature for success cases only.
			// Exception handler is responsible for signing failed cases
			return SignatureUtil.signResponseDto(response, mapper);
		
		} catch (CardApiException ex) {
			logger.error("Caught CardApiException !!" + ex.getMessage(), ex);
			throw ex; 
		} catch (Exception e) {
			logger.error("Caught Exception !!" + e.getMessage(), e);
			// default
			throw new CardApiException(ResponseConstant.AUTHENTICATION_ERROR); 
		}

	}
	
	private String getTransactionTimeInString(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(Constant.DATE_TIME_FORMAT);
		return formatter.format(date);
	}

	private String getExpiryTime(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat(Constant.DATE_FORMAT);
		return formatter.format(date);
	}
	
	private ResponseDto getDefaultResponse(RequestDto params, Date date) {
		ResponseDto response = new ResponseDto();
		response.setResp_code(ResponseConstant.SUCCESS.getResp_code());
		response.setResp_msg(ResponseConstant.SUCCESS.getResp_msg());
		
		ResponseDataDto responseData = new ResponseDataDto();
		// to make sure it's super unique
		responseData.setReference(params.getRequest_id() + "-"
				+ String.valueOf(date.getTime()));
		List<ResponseListCardDto> listCards = new ArrayList<ResponseListCardDto>();
		responseData.setList_card(listCards);
		
		response.setData(responseData);
		
		logger.info("setting default resp_code: {}, resp_msg: {} and reference: {}",
				response.getResp_code(), response.getResp_msg(), responseData.getReference());
		
		return response;
	}

	@Override
	public void reportAllSuccessfulTransactionsForYesterday() {
		
		SimpleDateFormat formatter = new SimpleDateFormat(Constant.FULL_TIME_FORMAT);
		Calendar cal = Calendar.getInstance();
		
		// performing query for yesterday
		cal.set(Calendar.DATE, cal.get(Calendar.DATE) - 1);
		
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		String before = formatter.format(cal.getTime());
		
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		String after = formatter.format(cal.getTime());
		
		logger.info("Using before time: {} and after time: {} to search for yesterday records", before, after);
		
		List<CardInfo> todayTransactions = cardInfoRepo.getAllSuccessfulTransactionsForYesterday(before, after, USED);
		logger.info("Sucessfully extracted [{}] records", todayTransactions.size());
		
		String filePath = FileUtils.saveFileToLocal(todayTransactions, cal.getTime());
		logger.info("Successfully saved to temp path: {}", filePath);
		
		FileUtils.uploadTextFileToFTPServer(filePath);
		logger.info("Successfully upload to FTP server");
	}
	
}
