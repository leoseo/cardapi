package com.card.api.CardAPI.service;

public interface BaseService {
	public final Integer UNUSED = 0;
	public final Integer USED = 1;
	public final Integer REFUNDED = 2;
}
