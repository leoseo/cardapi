package com.card.api.CardAPI.service;

import com.card.api.CardAPI.dto.RequestDto;
import com.card.api.CardAPI.dto.ResponseDto;

public interface CardService extends BaseService{
	
	public ResponseDto getResultAfterSendingCardPurcharsingRequest(RequestDto params);
	
	public ResponseDto getResultAfterSendingCardGettingRequest(RequestDto params);
	
	public ResponseDto getResultAfterSendingCardRefundRequest(RequestDto params);
	
	public void reportAllSuccessfulTransactionsForYesterday();
}
