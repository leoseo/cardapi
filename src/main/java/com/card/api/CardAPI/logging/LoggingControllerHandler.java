package com.card.api.CardAPI.logging;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
@Aspect
@Component
public class LoggingControllerHandler {

	Logger log = LoggerFactory.getLogger(this.getClass());

	private ObjectMapper mapper = new ObjectMapper();;
	@Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
	public void controller() {
	}

	@Pointcut("execution(public * *(..))")
	protected void allMethod() {
	}

	// before -> Any resource annotated with @Controller annotation
	// and all method and function taking HttpServletRequest as first parameter
	@Before("controller() && allMethod() && @annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public void logBefore(JoinPoint joinPoint) {

		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		RequestMapping myAnnotation = method.getAnnotation(RequestMapping.class);
		log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		log.info("Path:{}", AnnotationUtils.getValue(myAnnotation,"value"));
		
		log.info("Method :{}#{}",joinPoint.getSignature().getDeclaringTypeName(),joinPoint.getSignature().getName());
		Object[] args = joinPoint.getArgs();
		
		int i = 0;
		for (Object arg : args) {
			i++;
			try {
				log.info("Arg{}: {}",i,mapper.writeValueAsString(arg));
			} catch (JsonProcessingException e) {
				//do nothing
				//log.error(e.getMessage());
			}
		}
	}

	// After -> All method within resource annotated with @Controller annotation
	// and return a value
	@AfterReturning(pointcut = "controller() && allMethod()", returning = "result")
	public void logAfter(JoinPoint joinPoint, Object result) {
		try {
			log.info("Return value: {}",mapper.writeValueAsString(result));
		} catch (JsonProcessingException e) {
			//do nothing
			//log.error(e.getMessage());
		}
		
		log.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
	}

	// After -> Any method within resource annotated with @Controller annotation
	// throws an exception ...Log it
	@AfterThrowing(pointcut = "controller() && allMethod()", throwing = "exception")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable exception) {
		log.error("An exception has been thrown in " + joinPoint.getSignature().getName() + " ()");
		log.error("Cause : " + exception.getCause());
		log.error(exception.getMessage(), exception);
	}

	// Around -> Any method within resource annotated with @Controller annotation
	@Around("controller() && allMethod()")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
		Object result = joinPoint.proceed();
		if(log.isDebugEnabled()) {
			long start = System.currentTimeMillis();
			try {
				String className = joinPoint.getSignature().getDeclaringTypeName();
				String methodName = joinPoint.getSignature().getName();
				
				long elapsedTime = System.currentTimeMillis() - start;
				log.debug("Method " + className + "." + methodName + " ()" + " execution time : " + elapsedTime + " ms");
				
			} catch (IllegalArgumentException e) {
				log.error("Illegal argument " + Arrays.toString(joinPoint.getArgs()) + " in "
						+ joinPoint.getSignature().getName() + "()");
				throw e;
			}
		}
		
		return result;
	}
}