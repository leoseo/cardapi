package com.card.api.CardAPI.util;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;

import com.card.api.CardAPI.constant.ResponseConstant;
import com.card.api.CardAPI.exception.CardApiException;

public class RSAUtilsVer3 {
	
	private static final String SIGNATURE_ALGORITHM_SHA1_WITH_RSA = "SHA1withRSA";
//	private static final String OAEP_PADDING = "RSA/ECB/OAEPWithSHA1AndMGF1Padding";
	private static final String RSA = "RSA";
	
	public static PrivateKey loadPrivateKey(String filename) throws IOException {
        byte[] keyBytes = readAllBytes(filename);
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);

        try {
            KeyFactory kf = KeyFactory.getInstance(RSA);
            return kf.generatePrivate(spec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new UnknownError(e.getMessage());
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            throw new UnknownError(e.getMessage());
        }
    }

    public static PublicKey loadPublicKey(String filename) 
    		throws CardApiException, IOException {
        byte[] keyBytes = readAllBytes(filename);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);

        try {
            KeyFactory kf = KeyFactory.getInstance(RSA);
            return kf.generatePublic(spec);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new UnknownError(e.getMessage());
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            throw new UnknownError(e.getMessage());
        }
    }

    public static String sign(String privateKeyPath, String data)
            throws IOException, InvalidKeyException, SignatureException {
        byte[] message = data.getBytes();
        try {
            PrivateKey privateKey = loadPrivateKey(privateKeyPath);
            Signature signer = Signature.getInstance(SIGNATURE_ALGORITHM_SHA1_WITH_RSA);
            signer.initSign(privateKey);
            signer.update(message);
            byte[] signature = signer.sign();
            byte[] base64_encoded_signature = Base64.encodeBase64(signature);
            return new String(base64_encoded_signature);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new UnknownError(e.getMessage());
        }
    }

    public static boolean verify(String publicKeyPath, String data, String signature)
            throws CardApiException, IOException, InvalidKeyException, SignatureException {
        byte[] message = data.getBytes();
        byte[] base64_decoded_signature = Base64.decodeBase64(signature);
        try {
            PublicKey publicKey = loadPublicKey(publicKeyPath);
            Signature verifier = Signature.getInstance(SIGNATURE_ALGORITHM_SHA1_WITH_RSA);
            verifier.initVerify(publicKey);
            verifier.update(message);
            return verifier.verify(base64_decoded_signature);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new UnknownError(e.getMessage());
        }
    }
    
    public static byte[] readAllBytes(String filename) throws CardApiException, IOException  {
        File f = new File(filename);
        FileInputStream fis = null;
        DataInputStream dis = null;
        try {
			fis = new FileInputStream(f);
			dis = new DataInputStream(fis);
			byte[] keyBytes = new byte[(int) f.length()];
			dis.readFully(keyBytes);
			return keyBytes;
        } catch (FileNotFoundException ex) {
	        	ex.printStackTrace();
	    	 	// cannot find partner ID
        		throw new CardApiException(ResponseConstant.PARTNER_NOT_FOUND);
        } finally {
            if (dis != null)
                dis.close();
            if (fis != null)
                fis.close();
        }
    }
    
    public static byte[] encrypt(PublicKey key, byte[] plaintext) 
			throws NoSuchAlgorithmException, NoSuchPaddingException, 
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
	    Cipher cipher = Cipher.getInstance(RSA);   
	    cipher.init(Cipher.ENCRYPT_MODE, key);  
	    return cipher.doFinal(plaintext);
	}

	public static byte[] decrypt(PrivateKey key, byte[] ciphertext)
			throws NoSuchAlgorithmException, NoSuchPaddingException, 
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
	    Cipher cipher = Cipher.getInstance(RSA);   
	    cipher.init(Cipher.DECRYPT_MODE, key);  
	    return cipher.doFinal(ciphertext);
	}

}
