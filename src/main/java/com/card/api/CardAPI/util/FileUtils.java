package com.card.api.CardAPI.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.card.api.CardAPI.configs.ApplicationConfig;
import com.card.api.CardAPI.constant.Constant;
import com.card.api.CardAPI.constant.ResponseConstant;
import com.card.api.CardAPI.dto.TextFileDto;
import com.card.api.CardAPI.entity.CardInfo;
import com.card.api.CardAPI.exception.CardApiException;

public class FileUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);
	/*public static String _ftpServerDomain;
	public static Integer _ftpServerPort;
	public static String _ftpUsername;
	public static String _ftpPassword;*/
	
	private final static String COMMA_DELIMITER = ",";
	private final static String OR_DELIMITER = "|";
	//yyyymmdd_TRAN_<Partner Name>.txt
	private final static String FILE_FORMAT = "{0}_TRAN_{1}.txt";
	
	/**
	 * Util method to read temp file and upload to ftp server
	 * @param fileName tmp file name
	 */
	public static void uploadTextFileToFTPServer (String filePath) {
		
		FTPClient client = new FTPClient();
		InputStream is = null;
		boolean result = false;
		File file = null;
		
		logger.info("Receiving full path: {}", filePath);
		try {
		    client.connect(ApplicationConfig.ftpServerDomain, ApplicationConfig.ftpServerPort);
		    client.login(ApplicationConfig.ftpUsername, ApplicationConfig.ftpPassword);
		    logger.info("FTP connection - ftpServerDomain: {}, ftpServerPort: {} and ftpUsername: {}",
		    		ApplicationConfig.ftpServerDomain, ApplicationConfig.ftpServerPort, ApplicationConfig.ftpUsername);
		    
		    // Create an InputStream of the file to be uploaded
		    file = new File(filePath);
		    is = new FileInputStream(file);
		    
		    // Store file to server
		    result = client.storeFile(file.getName(), is);
		   logger.info("Uploading result: {} for currentTime: {}",
				   result, new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));
		   
		    client.logout();
		} catch (IOException e) {
			logger.error("Caught IOException: " +  e.getMessage(), e); 
			result = false;
		} finally {
		    try {
		        if (is != null) {
		            is.close();
		        }
		        client.disconnect();
			    
			    if (!result) {
			    		throw new CardApiException(ResponseConstant.FILE_UPLOAD_ERROR);
			    } else {
			    		logger.info("Deleting temp files in {} since uploading successfully",
			    				file.getAbsolutePath());
			    		file.delete();
			    }
		    } catch (Exception e) {
		    		logger.error("Caught Exception: " +  e.getMessage(), e);
		    		// probably saved successfully but might have memory leak
		    		// but ignore anyway
		    }
		    
		}
	}
	
	public static String saveFileToLocal(List<CardInfo> datas, Date date) throws CardApiException {
		
		String filePath = getFilePathForDate(date);
		String randomString = getRandomString();
		
		// prepare header line
		StringBuilder header = new StringBuilder();
		Field f[] = TextFileDto.class.getDeclaredFields();
		for (int i=0; i < f.length; i++) {
			String variableName = f[i].getName();
			logger.info("Reading variable: {}", variableName);
			header.append(variableName);
			if (i < f.length -1) {
				header.append(COMMA_DELIMITER);
			}
		}
		createAndWriteToANewFile(header.toString(), filePath);
		
		SimpleDateFormat sdf = new SimpleDateFormat(Constant.DATE_TIME_FORMAT);
		// prepare body
		for (CardInfo data: datas) {
			StringBuilder body = new StringBuilder();
			
			// trans_time
			body.append(sdf.format(new Date(data.getTransactionDate().getTime())));
			body.append(OR_DELIMITER);
			
			// product_code
			body.append(data.getCardName());
			body.append(OR_DELIMITER);
			
			/// amount
			body.append(data.getUnitPrice());
			body.append(OR_DELIMITER);
			
			// partner reference no
			body.append(data.getSerialNumber());
			body.append(OR_DELIMITER);
			
			// reference no
			body.append(data.getReferenceNo());
			body.append(OR_DELIMITER);
			
			// AirPay ResponseCode
			body.append("");
			body.append(OR_DELIMITER);
			
			// keeping mpd5 temp
			String checksum = getMD5Hash(body.toString(), randomString);
			
			// mismatch data
			body.append("");
			body.append(OR_DELIMITER);
			
			// md5 checksum
			body.append(checksum);
			
			writeToExistingFile(body.toString(), filePath);
		}
		
		// prepare ending
		Map<String , Object> tmpMap = new LinkedHashMap<String, Object>();
		StringBuilder footer = new StringBuilder();
		
		// total transaction amount
		Integer size = datas.size();
		tmpMap.put("size", size);
		footer.append(size);
		footer.append(OR_DELIMITER);
		
		// sender name
		tmpMap.put("sender", ApplicationConfig.partnerName);
		footer.append(ApplicationConfig.partnerName);
		footer.append(OR_DELIMITER);
		
		// sent time
		String sentTime = new SimpleDateFormat(Constant.DATE_TIME_FORMAT).format(date);
		tmpMap.put("sentTime", sentTime);
		footer.append(sentTime);
		footer.append(OR_DELIMITER);
		
		// checksum md5
		footer.append(getEndLineMD5Hash(tmpMap, randomString));
		
		writeToExistingFile(footer.toString(), filePath);
		
		return filePath;
	}
	
	private static String getFilePathForDate(Date today) {
		String fileName = MessageFormat.format(FILE_FORMAT,
				new SimpleDateFormat(Constant.PLAIN_DATE_FORMAT).format(today),
				ApplicationConfig.partnerName);
		String filePath = ApplicationConfig.storedFilePath + File.separator + fileName;
		logger.info("Using filePath: {}", filePath);
		return filePath;
	}
	
	private static void createAndWriteToANewFile(String line, String filePath) 
			throws CardApiException{
		BufferedWriter bw = null;
		FileWriter fw = null;
		logger.info("Saving to {} ", filePath);
		
		try {
			fw = new FileWriter(filePath);
			bw = new BufferedWriter(fw);
			bw.write(line + "\r\n");
		} catch (IOException ex) {
			logger.error("Exception occurred while saving the file ", ex);
			throw new CardApiException(ResponseConstant.FILE_SAVE_ERROR);
		} finally {
			logger.info("gonna close bw after using");
			try {
				if (bw != null)
					// fw is closed inside
					bw.close();
			} catch (IOException ex) {
				logger.error("Exception occurred while closing buffer writer ", ex);
				// leaking resource. ignore temporarily
			}
			
		}
	}
	
	private static void writeToExistingFile(String line, String filePath) {
		BufferedWriter bw = null;
		FileWriter fw = null;
		PrintWriter out = null;
		logger.info("Continue writing to {} ", filePath);
		
		try {
			fw = new FileWriter(filePath, true);
			bw = new BufferedWriter(fw);
			out = new PrintWriter(bw);
			out.println(line);
		} catch (IOException ex) {
			logger.error("Exception occurred while saving the file ", ex);
			// allowing to continue writing
			//throw new CardApiException(ResponseConstant.FILE_SAVE_ERROR);
		} finally {
			try {
				if (bw != null)
					// fw is closed inside
					bw.close();
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				logger.error("Exception occurred while closing buffer writer ", ex);
				// leaking resource. ignore temporarily
			}
			
		}
	}
	
	private static String getMD5Hash(String input, String randomString) {
		logger.debug("getMD5Hash - before replacing and md5Hex: {}, randomString: {}" , input, randomString);
		String anotherInput = input.replaceAll("\\|", "") + randomString;
		logger.debug("getMD5Hash - after replacing + appending: {}", anotherInput);
		String result = DigestUtils.md5Hex(anotherInput);
		logger.debug("getMD5Hash - final result: {}" , result);
	    return result;
	}
	
	private static String getEndLineMD5Hash (Map<String, Object> inputs, String randomString) {
		StringBuilder sb = new StringBuilder();
		sb.append(inputs.get("sentTime"));
		sb.append(inputs.get("size"));
		sb.append(inputs.get("sender"));
		// discussed that a random string would be inserted
		sb.append(randomString);
		logger.debug("getEndLineMD5Hash - before md5Hex: {}" , sb.toString());
		String result = DigestUtils.md5Hex(sb.toString());
		logger.debug("getEndLineMD5Hash - after md5Hex: {}" , result);
		return result;
	}
	
	private static String getRandomString() {
		return System.getenv().get("RANDOM_STRING");
	}
	
//	public static void main(String[] args) {
//		_ftpServerDomain = "123.31.27.50";
//		_ftpServerPort = 21;
//		_ftpUsername = "testapi";
//		_ftpPassword = "123@123a";
//		uploadTextFileToFTPServer("/Users/leoseo/Desktop/Environment/CustomProject/CardAPI/files/20180210_TRAN_VPAY.txt");
//	}

}
