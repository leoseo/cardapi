package com.card.api.CardAPI.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.card.api.CardAPI.configs.ApplicationConfig;
import com.card.api.CardAPI.dto.RequestDto;
import com.card.api.CardAPI.dto.ResponseDto;
import com.card.api.CardAPI.exception.CardApiException;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SignatureUtil {

	private static final Logger logger = LoggerFactory.getLogger(SignatureUtil.class);
	
	private static ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
	}
	
	/**
	 * Util method to build a signature for every request
	 * 
	 * @param dto
	 * @param mapper
	 * @return
	 * @throws IOException
	 */
	public static ResponseDto signResponseDto(ResponseDto dto, ObjectMapper mapper) throws IOException {
		String json = mapper.writeValueAsString(dto);
		logger.info("Input to generate signature: {}", json);
		String signature = SignatureUtil.getFinalSignatureAfterSigning(json);
		dto.setSignature(signature);
		logger.info("Generated signature: {}", signature);
		return dto;
	}

	/**
	 * Real util method to build a signature for every request
	 * 
	 * @param initialJson
	 * @return the signature
	 */
	public static String getFinalSignatureAfterSigning(String initialJson) {
		String result = null;
		try {
			logger.info("Before - initial JSON string" + initialJson);

			// 1st step: sorting
			String firstTemp = getSortedJsonString(initialJson);
			logger.info("1st - JSON string after sorting keys in the lexicographical order: " + firstTemp);

			// 2nd + 3rd step: SHA1withRSA + sign by private key + encode in base 64
			String privateKeyPath = ApplicationConfig.rsaKeyPairPath + File.separator
					+ ApplicationConfig.privateKeyNameDer;
			result = RSAUtilsVer3.sign(privateKeyPath, firstTemp);
			logger.info("2nd & 3rd - string after encoded in Base64: " + result);

		} catch (Exception ex) {
			logger.error("Caught exception !!!" + ex.getMessage(), ex);
		}

		return result;
	}
	
	/**
	 * Util method to verify a signature for every request
	 * @param requestContent
	 * @return
	 * @throws CardApiException
	 * @throws Exception
	 */
	public static boolean verifySignatureAndData(RequestDto requestContent) 
			throws CardApiException, Exception {
		boolean result = false;
		if (requestContent != null && requestContent.getSignature() != null
				&& requestContent.getPartner_id() != null) {
			
			// keeping first then set null
			String signature = requestContent.getSignature();
			requestContent.setSignature(null);

			// to write non-null value only
			String dataToVerify = mapper.writeValueAsString(requestContent);
			logger.info("Data to verify: {}", dataToVerify);

			// 1st step: sorting
			String firstTemp = getSortedJsonString(dataToVerify);
			logger.info("1st - JSON string after sorting keys in the lexicographical order: " + firstTemp);

			String publicKeyPath = ApplicationConfig.defaultPartnerKeyPath + File.separator
					+ requestContent.getPartner_id() + File.separator + ApplicationConfig.publicKeyName;
			result = RSAUtilsVer3.verify(publicKeyPath, firstTemp, signature);
			logger.info("2nd - result after verifying " + String.valueOf(result));
		}

		return result;
	}

	/**
	 * Step 1.1 or step 2.1 - sort Json String by keys in alphabetical order (sorted even nested json inside)
	 * 
	 * @param json
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public static String getSortedJsonString(String json) throws JsonParseException, JsonMappingException, IOException {
		Map<String, String> tempMap = new HashMap<String, String>();
		tempMap = mapper.readValue(json, Map.class);
		return json_dumps(tempMap, true);
	}
	
	@SuppressWarnings("rawtypes")
	public static String json_dumps(Map map, boolean keysSorted) throws JsonProcessingException {
        return json_dumps(map, keysSorted, ":", ",", new StringBuilder());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static String json_dumps(Map map, boolean keysSorted, String keyValueSeparator, String itemsSeparator, StringBuilder sb)
			throws JsonProcessingException {
        List<String> keys = new ArrayList<String>(map.keySet());
        if (keysSorted) {
            Collections.sort(keys);
        }

        sb.append('{');

        boolean first = true;
        for (String key : keys) {
            if (first) {
                first = false;
            } else {
                sb.append(itemsSeparator);
            }

            if (key == null) {
                sb.append("\"null\"");
            } else {
                sb.append(JSONValue.toJSONString(key));
            }

            sb.append(keyValueSeparator);
            toJSONString(map.get(key), keysSorted, keyValueSeparator, itemsSeparator, sb);
        }

        sb.append('}');

        return sb.toString();
    }
	
	@SuppressWarnings("rawtypes")
	private static void toJSONString(Object value, boolean keysSorted, String keyValueSeparator, String itemsSeparator, StringBuilder sb) 
			throws JsonProcessingException {
        if (value instanceof List) {
            sb.append('[');
            boolean first = true;
            for (Object item : (List) value) {
                if (first) {
                    first = false;
                } else {
                    sb.append(itemsSeparator);
                }
                toJSONString(item, keysSorted, keyValueSeparator, itemsSeparator, sb);
            }
            sb.append(']');
        } else if (value instanceof Map) {
            json_dumps((Map) value, keysSorted, keyValueSeparator, itemsSeparator, sb);
        } else {
//            sb.append(JSONValue.toJSONString(value));
        		// changing to using objectMapper 
        		// since JSONValue.toJSONString() is considering replacing / = \\/
        		// / is not an escape character
        		sb.append(mapper.writeValueAsString(value));
        }
    }
	
//	public static boolean testVerifySignature(String json, String signature) {
//		
//		try {
//			// 1st step: sorting
//			String firstTemp = getSortedJsonString(json);
//			logger.info("1st - JSON string after sorting keys in the lexicographical order: " + firstTemp);
//
//			String publicKeyPath = ApplicationConfig.defaultPartnerKeyPath + File.separator
//					+ "AIRPAY" + File.separator + ApplicationConfig.publicKeyName;
//			return RSAUtilsVer3.verify(publicKeyPath, firstTemp, signature);
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		return false;
//	}
	
//	public static void main(String[] args) throws Exception {
//		String test = "{\"resp_code\":0,\"resp_msg\":\"Success\",\"data\":{\"reference\":\"1533032835727\",\"list_card\":[{\"reference_no\":\"ved_000001\",\"trans_time\":\"2018-07-31 17:27:15\",\"serial\":\"072791000000111\",\"pin\":\"bRzjFgGe3bPrIH+Ub7ILmEax8OZ/uBOFzJWLw/ospDqx2axtkETPZ/ll8NN1oUxDZbcZYvtqM1LUbJ6qTgpYur1DTyS9Cgd4PZGIBzDLb8sSen+7lfbcy8OkMN0v7V9kunN8D2XpFkS5bHOhBsXdcl6F7XF2IZ9gdaBmY9/le7qnbvyt+RfopOs6EJTJHCfa3yZDkPhK1HTgIbzk6C3ZDNitjzXbjnmWkS2peV11MITv8lf9bNPVD5fcCXTz5m/4FAY/uWQSO346wmHATv+wqCJPmGbDecLdnp1BSqtne3QDu4ckuwjATqfcQp7HNeDvklpsIxDThABCgRbiFqCFSA==\",\"expiry\":\"2020-12-31\",\"error\":0},{\"reference_no\":\"ved_000002\",\"trans_time\":\"2018-07-31 17:27:15\",\"serial\":\"072791000011111\",\"pin\":\"EjqC6n3A8E0hyCxhWFTFNLzr8wfh8zq5zdAtrcqgVZ3zuDb5zyHcMQBAoHNK8M1+8IMZ15qTDxAR+sdvtvsCqPQbcZPpjTH/jhV/i+3dy2oXkGkcz5TNGxWjqMrUNCaz/X5rt4gHkp1ftbBvI0WkPC458CUBtDj7khRsjn+R+BImGeZdeGvqOYsPy+iQMNB4X+l83pXUNAOkG4CAyitLY56+TdDwxuZn0vCw4noqyOIXO7gZFw7w9ufW9N7T57wSwUbkfq5wzfKcBWm+QsDSdoG6sEY48Ic9HymAq5FQUIDuS5srFS7wn9KVvElfJtILvO3ZToY7CGvJYz550yG4LA==\",\"expiry\":\"2020-12-31\",\"error\":0}]}}";
//		System.out.println("before: " + test);
//		String sortedTest = getSortedJsonString(test);
//		System.out.println("after: " + sortedTest);
//	}

}
