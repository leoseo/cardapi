package com.card.api.CardAPI.util;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
//import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.card.api.CardAPI.configs.ApplicationConfig;
import com.card.api.CardAPI.dto.RequestDto;
import com.card.api.CardAPI.exception.CardApiException;

public class PinUtil {
	
	private final static Logger logger = LoggerFactory.getLogger(PinUtil.class);
	
	public static String getEncriptedPinCode(RequestDto request, String pinCode) 
			throws CardApiException, NoSuchAlgorithmException, InvalidKeySpecException, IOException, 
			InvalidKeyException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		
		String defaultKeyPath = ApplicationConfig.defaultPartnerKeyPath
				+ File.separator + request.getPartner_id() + File.separator + ApplicationConfig.publicKeyName;
		logger.info("using defaultKeyPath: {}", defaultKeyPath);
		PublicKey publicKey = RSAUtilsVer3.loadPublicKey(defaultKeyPath);
		logger.info("Finished loading public key");
		byte[] message = pinCode.getBytes("UTF8");
		logger.info("Gonna encrypt this pin: {} in UTF8", message);
		byte[] secret = RSAUtilsVer3.encrypt(publicKey, message);
		logger.info("Finished encrypting the pin code. Now encode to Base64");
		String result = new String(Base64.encodeBase64(secret));
		logger.info("Final Base64 string of pinCode: {} is {} ", pinCode, result);
		return result;
	}
	
//	public static void main(String[] args) throws Exception {
//		//prepare
//		RequestDto dto = new RequestDto();
//		dto.setPartner_id("AIRPAY_1");
//		String pinCode = "2MJMFTHMJ2TJ";
//		
//		// test
//		String defaultKeyPath = "/Users/leoseo/Desktop/Environment/MyRepository/CardAPI/src/main/resources/keys/others"
//				+ File.separator + dto.getPartner_id();
//		PublicKey publicKey = RSAUtilsVer3.loadPublicKey(defaultKeyPath + File.separator + "public.der");
//		PrivateKey privateKey = RSAUtilsVer3.loadPrivateKey(defaultKeyPath + File.separator + "private.der");
//        byte[] message = pinCode.getBytes("UTF8");
//        System.out.println("#1 - message is: " + new String(message, "UTF8"));
//        byte[] secret = RSAUtilsVer3.encrypt(publicKey, message);
//        System.out.println("#2 - secret is: " + new String(secret, "UTF8"));
//        String base64Pin =  new String(Base64.encodeBase64(secret));
//        System.out.println("#3 - base64Pin: " + base64Pin);
//        byte[] recovered_message = RSAUtilsVer3.decrypt(privateKey, secret);
//        System.out.println("#4 - recovered_message is: " + new String(recovered_message, "UTF8"));
//	}

}
